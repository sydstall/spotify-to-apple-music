const fs = require('fs');
const dotenv = require('dotenv');

const express = require('express');

const appleMusicService = require('../services/appleMusicService.js');

const router = express.Router();


router
  .post('/songs/search', async function(req, res) {
	const token = req.body.token;
	const songs = req.body.playlist.songs;

	dotenv.config();

	const appleKeyId = process.env.APPLE_KEY_ID;
	const appleIssuerId = process.env.APPLE_ISSUER_ID;
	const privateKeyFile = './AuthKey_' + appleKeyId + '.p8';
	const applePrivateKey = fs.readFileSync(privateKeyFile, 'utf8');

	const appleMusic = new appleMusicService(applePrivateKey, appleKeyId, appleIssuerId);

	res.setHeader('Content-Type', 'application/json');

	appleMusic.searchForSongs(token, songs, function(matches) {
	  res.send(JSON.stringify({ matches: matches }));
	});
  });


router
  .post('/playlists/create', function(req, res) {
	const developerToken = req.body.developerToken;
	const userToken = req.body.userToken;
	const playlistName = req.body.playlistName;
	const playlistSongs = req.body.playlistSongs;

	const appleKeyId = process.env.APPLE_KEY_ID;
	const appleIssuerId = process.env.APPLE_ISSUER_ID;
	const privateKeyFile = './AuthKey_' + appleKeyId + '.p8';
	const applePrivateKey = fs.readFileSync(privateKeyFile, 'utf8');

	const appleMusic = new appleMusicService(applePrivateKey, appleKeyId, appleIssuerId);

	res.setHeader('Content-Type', 'application/json');

	appleMusic.createPlaylist(developerToken, userToken, playlistName, playlistSongs, function(response) {
	  res.send(JSON.stringify(response));
	});
  });

module.exports = router;