const fs = require('fs');
const dotenv = require('dotenv');

const express = require('express');

const spotifyService = require('../services/spotifyService.js');
const appleMusicService = require('../services/appleMusicService.js');

const router = express.Router();


router
  .get('/apple-music/tokens/create', function(req, res) {
	  dotenv.config();

	  const appleKeyId = process.env.APPLE_KEY_ID;
	  const appleIssuerId = process.env.APPLE_ISSUER_ID;
	  const privateKeyFile = './AuthKey_' + appleKeyId + '.p8';
	  const applePrivateKey = fs.readFileSync(privateKeyFile, 'utf8');

	  const appleMusic = new appleMusicService(applePrivateKey, appleKeyId, appleIssuerId);

	  const token = appleMusic.generateToken();

	  res.setHeader('Content-Type', 'application/json');
	  res.send(JSON.stringify({ token: token }));
  });

router
  .get('/spotify/tokens/create', function(req, res) {
	  dotenv.config();

	  const spotifyClientId = process.env.SPOTIFY_CLIENT_ID;
	  const spotifyClientSecret = process.env.SPOTIFY_CLIENT_SECRET;

	  const spotify = new spotifyService(spotifyClientId, spotifyClientSecret);

	  spotify.getDeveloperToken(function(token) {
	    res.setHeader('Content-Type', 'application/json');
	    res.send(JSON.stringify({ token: token }));
	  });
  });

router
  .get('/spotify/client-id/retrieve', function(req, res) {
  	  dotenv.config();

  	  res.send(JSON.stringify({ clientId: process.env.SPOTIFY_CLIENT_ID }));
  });


module.exports = router;