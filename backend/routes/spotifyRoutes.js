const dotenv = require('dotenv');

const express = require('express');

const spotifyService = require('../services/spotifyService.js');

const router = express.Router();


router
  .post('/playlists/retrieve', function(req, res) {
	const spotifyDeveloperToken = req.body.token;
	const playlistId = req.body.playlistId;

	dotenv.config();

	const spotifyClientId = process.env.SPOTIFY_CLIENT_ID;
	const spotifyClientSecret = process.env.SPOTIFY_CLIENT_SECRET;

	const spotify = new spotifyService(spotifyClientId, spotifyClientSecret);

	spotify.getPlaylist(spotifyDeveloperToken, playlistId, function(result) {
	  const playlist = result;
	  const name = playlist.getName();
	  const imageUrl = playlist.getImage();
	  const songs = [];

	  for (let i = 0; i < playlist.getSongs().length; i++) {
	    songs.push(playlist.getSongs()[i].getDict());
	  }

	  res.setHeader('Content-Type', 'application/json');
	  res.send(JSON.stringify({ name: name, imageUrl: imageUrl, songs: songs }));
	});
  });

  router
    .post('/playlists/user/retrieve', function(req, res) {
      const spotifyToken = req.body.token;

      dotenv.config();

	  const spotifyClientId = process.env.SPOTIFY_CLIENT_ID;
	  const spotifyClientSecret = process.env.SPOTIFY_CLIENT_SECRET;

	  const spotify = new spotifyService(spotifyClientId, spotifyClientSecret);

	  spotify.getUserPlaylists(spotifyToken, function(result) {
		res.setHeader('Content-Type', 'application/json');
		res.send(JSON.stringify({ playlists: result }));
	  });
    });

module.exports = router;