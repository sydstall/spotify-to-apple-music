const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const cors = require('cors');

const authorizationRoutes = require('./routes/authorizationRoutes');
const spotifyRoutes = require('./routes/spotifyRoutes');
const appleMusicRoutes = require('./routes/appleMusicRoutes');

const app = express();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors());

app.use('/auth', authorizationRoutes);
app.use('/spotify', spotifyRoutes);
app.use('/apple-music', appleMusicRoutes);


app.get('/', function(req, res) {
  res.send(JSON.stringify('Hello, World!'));
});


app.listen(3000);