const fetch = require('node-fetch');
var jwt = require('jsonwebtoken');

const songModel = require('../models/song.js');

module.exports = class AppleMusicService {
  constructor(privateKey, keyId, issuerId) {
    this.privateKey = privateKey;
    this.keyId = keyId;
    this.issuerId = issuerId;
    this.apiUrl = 'https://api.music.apple.com/v1/';
  }

  generateToken() {
    return jwt.sign({}, this.privateKey, {
      algorithm: 'ES256',
      expiresIn: '180d',
      issuer: this.issuerId,
      header: {
        alg: 'ES256',
        kid: this.keyId
      }
    });
  }

  async searchForSongs(token, songs, callback) {
    /*
    GET /catalog/{storefront}/search?term={song+name}
    storefront=us (United States)
    */
    const matches = await Promise.all(songs.map(async song => {
      const encoded = encodeURIComponent(song.name + ' ' + song.artist);
      const url = this.apiUrl + 'catalog/us/search?term=' + encoded + '&types=songs';

      const match = await fetch(url, {
        'method': 'GET',
        'headers': {
          'Authorization': 'Bearer ' + token,
          'Content-Type': 'application/json'
        },
      })
        .then(res => res.json())
        .then(json => {
          if (json.message === 'API capacity exceeded') {
            return {
              'id': null,
              'name': null,
              'artist': null,
              'album': null
            };
          } else {
            if (Object.keys(json.results).length === 0 && json.results.constructor === Object) {
              return {
                'id': null,
                'name': null,
                'artist': null,
                'album': null
              };
            } else if ('error' in json) {
              return {
                'id': null,
                'name': null,
                'artist': null,
                'album': null
              };
            } else {
              const results = json.results.songs.data;

              let songMatch = new songModel(null, null, null, null);

              for (let i = 0; i < results.length; i++) {
                if (results[i].attributes.name === song.name && results[i].attributes.artistName === song.artist) {
                  songMatch = new songModel(
                    results[i].id,
                    results[i].attributes.name,
                    results[i].attributes.artistName,
                    results[i].attributes.albumName
                  );
                }
              }

              return songMatch.getDict();
            }
          }
        })
        .catch(error => { throw error; });

        return match;
    }));

    const exactMatches = [];

    for (let i = 0; i < matches.length; i++) {
      if (matches[i].name !== null) {
        exactMatches.push(matches[i]);
      }
    }

    callback(exactMatches);
  }

  async createPlaylist(developerToken, userToken, name, songs, callback) {
    /*
    POST /me/library/playlists
    */
    const payload = {
      'attributes': {
        'name': name,
        'description': ''
      },
      'relationships': {
        'tracks': {
          'data': []
        }
      }
    };

    for (let i = 0; i < songs.length; i++) {
      payload.relationships.tracks.data.push({
        'id': songs[i].id,
        'type': 'songs'
      });
    }

    await fetch(this.apiUrl + 'me/library/playlists', {
      'method': 'POST',
      'headers': {
        'Authorization': 'Bearer ' + developerToken,
        'Music-User-Token': userToken
      },
      'body': JSON.stringify(payload)
    })
      .then(res => res.json())
      .then(response => {
        callback(response)
      })
      .catch(error => { throw error; });
  }
};