module.exports = class Song {
  constructor(id, name, artist, album) {
    this.id = id;
    this.name = name;
    this.artist = artist;
    this.album = album;
  }

  getId() {
    return this.id;
  }

  getName() {
    return this.name;
  }

  getArtist() {
    return this.artist;
  }

  getAlbum() {
    return this.album;
  }

  getDict() {
    return {
      id: this.id,
      name: this.name,
      artist: this.artist,
      album: this.album
    };
  }
};