module.exports = class Playlist {
  constructor(name, songs, image) {
  	this.name = name;
	  this.songs = songs;
    this.image = image;
  }

  getName() {
	  return this.name;
  }

  getSongs() {
    return this.songs;
  }

  getImage() {
    return this.image;
  }
};