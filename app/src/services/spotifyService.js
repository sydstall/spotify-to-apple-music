const request = require('request');

const song = require('../models/song.js');
const playlist = require('../models/playlist.js');

module.exports = class SpotifyService {
  constructor(clientId, clientSecret) {
    this.clientId = clientId;
    this.clientSecret = clientSecret;
    this.apiUrl = 'https://api.spotify.com/v1';
    this.developerAuthorizeUrl = 'https://accounts.spotify.com/api/token';
    this.scopes = 'user-read-private%20playlist-read-private%20playlist-read-collaborative';
    this.redirectUri = '';
  }

  getDeveloperToken(callback) {
    /*
    POST https://accounts.spotify.com/api/token
    Auth: Basic <base64 encoded clientId:clientSecret>
    */
    const data = this.clientId + ':' + this.clientSecret;

    const auth = {
      url: this.developerAuthorizeUrl,
      headers: {
      	'Authorization': 'Basic ' + (new Buffer.alloc(data.length, data).toString('base64'))
      },
      form: {
      	grant_type: 'client_credentials'
      },
      json: true
    };

    this.post(auth).then((body) => {
      callback(body.access_token);
    });
  }

  getPlaylist(token, id, callback) {
    /*
    GET https://api.spotify.com/v1/playlists/{id}
    */
    const auth = {
      url: this.apiUrl + '/playlists/' + id,
      headers: {
      	'Authorization': 'Bearer ' + token
      },
      json: true
    };

    this.get(auth).then((body) => {
      const items = body.tracks.items;
      const songs = [];

      for (let i = 0; i < items.length; i++) {
      	const songName = items[i].track.name;
      	const songId = items[i].track.id;
      	const songArtist = items[i].track.artists[0].name;
      	const songAlbum = items[i].track.album.name;

      	songs.push(new song(songId, songName, songArtist, songAlbum));
      }

      const newPlaylist = new playlist(body.name, songs, null);

      callback(newPlaylist);
    });
  }

  getUserPlaylists(token, callback) {
    const auth = {
      url: this.apiUrl + '/me/playlists',
      headers: {
        'Authorization': 'Bearer ' + token
      },
      json: true
    };

    this.get(auth).then((body) => {
      const items = body.items;
      const playlists = [];

      for (let i = 0; i < items.length; i++) {
        const playlistName = items[i].name;
        const playlistImage = items[i].images[0].url;
        const playlistId = items[i].id;

        playlists.push({
          'name': playlistName,
          'image': playlistImage,
          'id': playlistId
        });
      }

      callback(playlists);
    });
  }

  post(options) {
    return new Promise((resolve, reject) => {
      request.post(options, function(error, response, body) {
        if (error) reject(error);
          resolve(body);
       });
    });
  }

  get(options) {
    return new Promise((resolve, reject) => {
      request.get(options, function(error, response, body) {
        if (error) reject(error);
          resolve(body);
      });
    });
  }
};