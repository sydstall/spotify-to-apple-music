const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');

const authorizationRoutes = require('./src/routes/authorizationRoutes');
const spotifyRoutes = require('./src/routes/spotifyRoutes');
const appleMusicRoutes = require('./src/routes/appleMusicRoutes');

const app = express();

app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');

app.use('/auth', authorizationRoutes);
app.use('/spotify', spotifyRoutes);
app.use('/apple-music', appleMusicRoutes);


app.get('/', function(req, res) {
  res.sendFile(path.join(__dirname, '/index.html'))
});


app.listen(3000);