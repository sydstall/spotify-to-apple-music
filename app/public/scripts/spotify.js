$(document).ready(function() {
  $('.js-nav-spotify').addClass('selected');

  $('.js-spotify-login').click(function() {
  console.log('logging in to spotify...');
  // Get client ID from API
  fetch('/auth/spotify/client-id/retrieve')
    .then(spotifyIdResponse => spotifyIdResponse.json())
    .then(spotifyIdRes => {
      const clientId = spotifyIdRes.clientId;
      const redirectUri = 'http://localhost:3000';
      const scope = 'playlist-read-private playlist-read-collaborative user-library-read user-read-private';
      const responseType = 'token';
      const state = Math.floor(Math.random() * 500);

      const parameters = {
  		client_id: clientId,
  		redirect_uri: redirectUri,
  		scope: scope,
  		response_type: responseType,
  		state: state
  	  };

      const query = $.param(parameters);
			
	  document.location.href = 'https://accounts.spotify.com/authorize?' + query;
    });
  });

  // When a hash has been successfully returned from Spotify
  if (window.location.hash) {
  	$('.js-spotify-login').addClass('hidden');

	const responseArray = window.location.hash.substring(1).split('&');
	const parameters = {};

	for (let i = 0; i < responseArray.length; i++) {
	  const key = responseArray[i].split('=')[0];
      const value = responseArray[i].split('=')[1];

	  parameters[key] = value;
	}

	// Now we can access their data (using their access token)
	fetch('/spotify/playlists/user/retrieve', {
	  'method': 'POST',
	  'headers': {
	    'Content-Type': 'application/json'
	  },
	  'body': JSON.stringify({
	    'token': parameters.access_token
	  })
	})
	  .then(spotifyResponse => spotifyResponse.json())
	  .then(spotifyRes => {
	  	const defaultList = '<ul class="list js-default-option"></ul>';
	  	const defaultOption = '\
	  		<li>\
	  			<div class="option js-default-value">\
	  				Choose a Spotify playlist\
	  				<img src="/img/expand_more-24px.svg" alt="expand more" id="expand-more" />\
	  				<img src="/img/expand_less-24px.svg" alt="expand more" id="expand-less" class="hidden" />\
	  			</div>\
	  		</li>';

	  	const selectList = '<ul class="list js-select-list"></ul>';

	  	$('.js-select').append(defaultList);
	  	$('.js-default-option').append(defaultOption);

	  	$('.js-select').append(selectList);

	  	for (let i = 0; i < spotifyRes.playlists.length; i++) {
	  	  const playlist = spotifyRes.playlists[i];
		  $('.js-select-list').append(
		  	'<li>\
		  	   <div class="option__container">\
		  	     <img class="option__image" src="' + playlist.image + '" />\
		  	     <p class="option option__text js-select-option" id="' + playlist.id + '">' + playlist.name + '</p>\
		  	   </div>\
		  	 </li>'
		  );
	  	}

	  	$('.js-default-option').click(function() {
	  	  $('.js-select-list').toggleClass('active');
	  	  $('#expand-more').toggleClass('hidden');
	  	  $('#expand-less').toggleClass('hidden');
	  	})

	  	$('.option__container').click(function() {
	  	  $('.js-select-list').removeClass('active');
	  	  const playlistId = $(this).find('p:first').attr('id');
	  	  const playlistName = $(this).find('p:first').text();

	  	  $('#spotify-playlist').val(playlistId);
	  	  $('.js-default-value').text(playlistName);
	  	  $('.js-default-value').attr('id', playlistId);
	  	});
	  });
  }
});