$(document).on('musickitloaded', function() {
  fetch('/auth/apple-music/tokens/create')
    .then(appleTokenResponse => appleTokenResponse.json())
    .then(appleRes => {
      const music = MusicKit.configure({
        developerToken: appleRes.token,
        app: { name: 'SharePlaylists' }
      });

      $('.js-apple-login').click(function() {
        music.authorize().then(musicUserToken => {
          fetch('/auth/spotify/tokens/create')
            .then(spotifyTokenResponse => spotifyTokenResponse.json())
            .then(spotifyRes => {
              const playlistId = $('.js-default-value').attr('id');

              $('.loading__modal').addClass('show');

              fetch('/spotify/playlists/retrieve', {
                'method': 'POST',
                'headers': {
                  'Content-Type': 'application/json'
                },
                'body': JSON.stringify({
                  'token': spotifyRes.token,
                  'playlistId': playlistId
                })
              })
                .then(spotifyPlaylistResponse => spotifyPlaylistResponse.json())
                .then(spotifyPlaylistRes => {
                  // Match songs from Spotify to Apple Music
                  fetch('/apple-music/songs/search', {
                    'method': 'POST',
                    'headers': {
                      'Content-Type': 'application/json'
                    },
                    'body': JSON.stringify({
                      'token': appleRes.token,
                      'playlist': {
                        'name': spotifyPlaylistRes.name,
                        'songs': spotifyPlaylistRes.songs
                       }
                    })
                  })
                    .then(appleMusicResponse => appleMusicResponse.json())
                    .then(appleMusicRes => {
                      // Add each match to a new playlist in Apple Music
                      fetch('/apple-music/playlists/create', {
                        'method': 'POST',
                        'headers': {
                          'Content-Type': 'application/json'
                        },
                        'body': JSON.stringify({
                          'developerToken': appleRes.token,
                          'userToken': musicUserToken,
                          'playlistName': spotifyPlaylistRes.name,
                          'playlistSongs': appleMusicRes.matches
                        })
                      })
                        .then(appleMusicPlaylistResponse => appleMusicPlaylistResponse.json())
                        .then(appleMusicPlaylistRes => {
                          console.log(appleMusicPlaylistRes);
                          console.log('Finished!');
                          music.unauthorize();
                          $('.loading__modal').removeClass('show');
                        });
                    });
                });
            });
        });
      });
    });
});