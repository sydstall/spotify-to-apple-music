FROM node:12.2.0

RUN mkdir -p /home/dockeruser/app

WORKDIR /home/dockeruser/app

ENV PATH /home/dockeruser/app/node_modules/.bin:$PATH

COPY package.json /home/dockeruser/app/package.json
COPY angular.json /home/dockeruser/app/angular.json
COPY tsconfig.json /home/dockeruser/app/tsconfig.json
COPY tslint.json /home/dockeruser/app/tslint.json

COPY ./src /home/dockeruser/app/src

RUN npm install
RUN npm install -g @angular/cli@9.1.1

EXPOSE 4200

CMD ["ng", "serve", "--host", "0.0.0.0"]