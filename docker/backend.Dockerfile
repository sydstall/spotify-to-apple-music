FROM node:12.2.0

RUN mkdir -p /home/dockeruser/app

WORKDIR /home/dockeruser/app

ENV PATH /home/dockeruser/app/node_modules/.bin:$PATH

COPY package.json /home/dockeruser/app/package.json
COPY .eslintrc.json /home/dockeruser/app/.eslintrc.json

COPY . /home/dockeruser/app

RUN npm install

EXPOSE 3000

CMD ["npm", "start"]