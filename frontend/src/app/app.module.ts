import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { NgReduxModule, NgRedux } from '@angular-redux/store';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TopBarComponent } from './components/top-bar/top-bar.component';
import { NavBarComponent } from './components/nav-bar/nav-bar.component';
import { SpotifyToAppleComponent } from './components/spotify-to-apple/spotify-to-apple.component';
import { AppleToSpotifyComponent } from './components/apple-to-spotify/apple-to-spotify.component';
import { ShareWithFriendComponent } from './components/share-with-friend/share-with-friend.component';
import { LoadingComponent } from './components/loading/loading.component';

import { AuthReducer, InitialState, initialState } from './store/reducers';

@NgModule({
  declarations: [
    AppComponent,
    TopBarComponent,
    NavBarComponent,
    SpotifyToAppleComponent,
    AppleToSpotifyComponent,
    ShareWithFriendComponent,
    LoadingComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    NgReduxModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(ngRedux: NgRedux<InitialState>) {
    ngRedux.configureStore(AuthReducer, initialState);
  }
}
