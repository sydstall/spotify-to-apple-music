import { ActionTypes } from './actions';

export interface InitialState {
  appleMusicToken: String;
  spotifyToken: String;
}
export const initialState = {
  appleMusicToken: null,
  spotifyToken: null
};

export function AuthReducer(state = initialState, action) {
  switch (action.type) {
    case ActionTypes.AddAppleMusicToken:
      return {
        ...state,
        appleMusicToken: action.payload
      };

    case ActionTypes.RemoveAppleMusicToken:
      return {
        ...state,
        appleMusicToken: null
      };

    case ActionTypes.AddSpotifyToken:
      return {
        ...state,
        spotifyToken: action.payload
      };

    case ActionTypes.RemoveSpotifyToken:
      return {
        ...state,
        spotifyToken: null
      };

    default:
      return state;
  }
}