export enum ActionTypes {
  AddAppleMusicToken = '[Token] Add Apple Music token to store',
  AddSpotifyToken = '[Token] Add Spotify token to store',
  RemoveAppleMusicToken = '[Token] Remove Apple Music token from store',
  RemoveSpotifyToken = '[Token] Remove Spotify token from store',
};

export const AddAppleMusicToken = payload => {
  return {
    type: ActionTypes.AddAppleMusicToken,
    payload
  };
};

export const AddSpotifyToken = payload => {
  return {
    type: ActionTypes.AddSpotifyToken,
    payload
  };
};

export const RemoveAppleMusicToken = payload => {
  return {
    type: ActionTypes.RemoveAppleMusicToken,
    payload
  };
};

export const RemoveSpotifyToken = payload => {
  return {
    type: ActionTypes.RemoveSpotifyToken,
    payload
  };
};