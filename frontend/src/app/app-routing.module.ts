import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SpotifyToAppleComponent } from './components/spotify-to-apple/spotify-to-apple.component';
import { AppleToSpotifyComponent } from './components/apple-to-spotify/apple-to-spotify.component';
import { ShareWithFriendComponent } from './components/share-with-friend/share-with-friend.component';


const routes: Routes = [
  { path: 'spotify-to-apple', component: SpotifyToAppleComponent },
  { path: 'apple-to-spotify', component: AppleToSpotifyComponent },
  { path: 'share-with-friend', component: ShareWithFriendComponent },
  { path: '',   redirectTo: '/spotify-to-apple', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
