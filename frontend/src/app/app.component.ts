import { Component, OnInit } from '@angular/core';
import { NgRedux } from '@angular-redux/store';

import { InitialState } from './store/reducer';
import { AddAppleMusicToken, AddSpotifyToken } from './store/actions';

import { AuthService } from './services/auth/auth.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'frontend';
  appleMusicToken = null;
  spotifyToken = null;

  constructor(private authService: AuthService, private ngRedux: NgRedux<InitialState>) { }

  ngOnInit() {
  	this.authService.getAppleMusicToken()
      .subscribe((resp) => {
        this.appleMusicToken = resp.body.token;

        this.ngRedux.dispatch(AddAppleMusicToken(resp.body.token));
      });

    this.authService.getSpotifyToken()
      .subscribe((resp) => {
        this.spotifyToken = resp.body.token;

        this.ngRedux.dispatch(AddSpotifyToken(resp.body.token));
      });
  }
}
