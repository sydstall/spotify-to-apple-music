import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppleToSpotifyComponent } from './apple-to-spotify.component';

describe('AppleToSpotifyComponent', () => {
  let component: AppleToSpotifyComponent;
  let fixture: ComponentFixture<AppleToSpotifyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppleToSpotifyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppleToSpotifyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
