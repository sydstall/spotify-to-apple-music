import { Component, OnInit, OnChanges, Input } from '@angular/core';

// import { Playlist } from '../../models/playlist';

@Component({
  selector: 'app-loading',
  templateUrl: './loading.component.html',
  styleUrls: ['./loading.component.scss']
})
export class LoadingComponent implements OnInit, OnChanges {
  @Input() playlists: Array;

  constructor() {
  }

  ngOnInit(): void {
  }

  ngOnChanges(): void {
  	console.log('change');
  }

}
