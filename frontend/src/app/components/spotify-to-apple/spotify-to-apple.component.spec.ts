import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpotifyToAppleComponent } from './spotify-to-apple.component';

describe('SpotifyToAppleComponent', () => {
  let component: SpotifyToAppleComponent;
  let fixture: ComponentFixture<SpotifyToAppleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpotifyToAppleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpotifyToAppleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
