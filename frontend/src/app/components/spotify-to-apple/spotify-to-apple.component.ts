import { Component, OnInit } from '@angular/core';
import { Location, LocationStrategy, PathLocationStrategy } from '@angular/common';
import { NgRedux } from '@angular-redux/store';

import { InitialState } from '../../store/reducer';

import { environment } from '../../../environments/environment';

import { AuthService } from '../../services/auth/auth.service';
import { SpotifyService } from '../../services/spotify/spotify.service';
import { AppleMusicService } from '../../services/apple-music/apple-music.service';
import { MusicKitService } from '../../services/music-kit/music-kit.service';

@Component({
  selector: 'app-spotify-to-apple',
  templateUrl: './spotify-to-apple.component.html',
  styleUrls: ['./spotify-to-apple.component.scss'],
  providers: [Location, { provide: LocationStrategy, useClass: PathLocationStrategy }]
})
export class SpotifyToAppleComponent implements OnInit {
  location: Location;

  clientId = null;
  spotifyUserToken = null;
  spotifyDeveloperToken = null;
  appleMusicUserToken = null;
  appleMusicDeveloperToken = null;
  playlists = null;
  selectedPlaylists = [];
  uploading = false;

  constructor(
  	private authService: AuthService,
  	private spotifyService: SpotifyService,
    private appleMusicService: AppleMusicService,
    private musicKitService: MusicKitService,
    private ngRedux: NgRedux<InitialState>,
  	location: Location
  ) {
  	this.location = location;

    this.ngRedux
      .select('spotifyToken')
      .subscribe((token) => {
        this.spotifyDeveloperToken = token;
      });

    this.ngRedux
      .select('appleMusicToken')
      .subscribe((token) => {
        this.appleMusicDeveloperToken = token;
      });
  }

  ngOnInit() {
  	// Check if access token is in URL
  	const path = this.location.path(true);

  	if (path.includes('#')) {
  		this.spotifyUserToken = path.slice(path.indexOf('#') + 1).split('&')[0].split('=')[1];

  	// Get the user's playlists
		this.spotifyService.getUserPlaylists(this.spotifyUserToken)
	    .subscribe((resp) => {
	      this.playlists = resp.body.playlists;
	    });
  	}

  	this.authService.getSpotifyClientId()
      .subscribe((resp) => {
        this.clientId = resp.body.clientId;
      });
  }

  logInToSpotify() {
    // Redirect to Spotify authorization URL

    const clientId = this.clientId;
    const redirectUri = 'http://localhost:4200' + this.location.path();
    const scope = 'playlist-read-private playlist-read-collaborative user-library-read user-read-private';
    const responseType = 'token';
    const showDialog = 'true';
    const state = Math.floor(Math.random() * 1000);

  	const parameters = {
  	  client_id: clientId,
  	  redirect_uri: redirectUri,
  	  scope: scope,
  	  response_type: responseType,
  	  show_dialog: showDialog,
  	  state: state
  	};

  	const query = Object.keys(parameters)
  		.map(key => encodeURIComponent(key) + '=' + encodeURIComponent(parameters[key]))
  		.join('&');

  	window.location.href = environment.SPOTIFY_AUTH_URL + '?' + query;
  }

  uploadToAppleMusic() {
    this.uploading = true;

    this.musicKitService.configureMusicKit();

    this.musicKitService.musicKit.authorize().then(() => {
      this.appleMusicUserToken = this.musicKitService.musicKit.musicUserToken;

      for (let i = 0; i < this.selectedPlaylists.length; i++) {
        // Retrieve Spotify playlist from API
        console.log('Retrieving Spotify playlist from API...');
        this.spotifyService.getPlaylist(this.spotifyDeveloperToken, this.selectedPlaylists[i])
          .subscribe((spotifyPlaylist) => {
            let playlist = {
              name: spotifyPlaylist.body.name,
              songs: spotifyPlaylist.body.songs
            };

            // Search for songs through Apple Music API
            console.log('Searching for matches...');
            this.appleMusicService.searchForSongs(this.appleMusicDeveloperToken, playlist)
              .subscribe((appleSongMatches) => {
                const playlist = {
                  name: spotifyPlaylist.body.name,
                  songs: appleSongMatches.body.matches
                };

                // Create an Apple Music playlist for authorized user
                console.log('Creating Apple Music playlist with matches...');
                this.appleMusicService.createPlaylist(this.appleMusicDeveloperToken, this.appleMusicUserToken, playlist)
                  .subscribe((resp) => {
                    console.log(resp);
                    this.removePlaylist(this.selectedPlaylists[i]);
                  });
              });
          });
      }

      this.uploading = false;

    });
  }

  addPlaylist(playlist) {
    if (this.selectedPlaylists.includes(playlist)) {
      this.removePlaylist(playlist);
    } else {
      this.selectedPlaylists.push(playlist);
    }
  }

  removePlaylist(playlist) {
    let index = 0;

    for (let i = 0; i < this.selectedPlaylists.length; i++) {
      if (playlist.id === this.selectedPlaylists[i].id) {
        index = i;
      }
    }

    this.selectedPlaylists.splice(index, 1);
  }

  getPlaylistClass(playlist) {
    if (this.selectedPlaylists.includes(playlist)) {
      return 'playlist__image--selected';
    } else {
      return 'playlist__image--unselected';
    }
  }

}
