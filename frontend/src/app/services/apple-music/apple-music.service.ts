import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AppleMusicService {
  API_URL = environment.API_URL + '/apple-music';

  constructor(private httpClient: HttpClient) { }

  searchForSongs(token, playlist) {
  	const payload = {
  	  token: token,
  	  playlist: {
  	  	name: playlist.name,
  	  	songs: playlist.songs
  	  }
  	};

  	return this.httpClient.post(this.API_URL + '/songs/search', payload, { observe: 'response' });
  }

  createPlaylist(developerToken, userToken, playlist) {
    const payload = {
      userToken: userToken,
      developerToken: developerToken,
      playlistName: playlist.name,
      playlistSongs: playlist.songs
    };

    return this.httpClient.post(this.API_URL + '/playlists/create', payload, { observe: 'response' });
  }  
}
