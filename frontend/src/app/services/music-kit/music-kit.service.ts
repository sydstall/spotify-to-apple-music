import { Injectable } from '@angular/core';
import { NgRedux } from '@angular-redux/store';

import { InitialState } from '../../store/reducer';

declare var MusicKit: any;

@Injectable({
  providedIn: 'root'
})
export class MusicKitService {
  musicKit: any;

  appleMusicToken = null;
  authorized = false;

  constructor(private ngRedux: NgRedux<InitialState>) {
    this.ngRedux
      .select('appleMusicToken')
      .subscribe((token) => {
        this.appleMusicToken = token;
      });
  }

  configureMusicKit() {
    // Configure Apple MusicKit
    MusicKit.configure({
      developerToken: this.appleMusicToken,
      app: { name: 'SharePlaylists' }
    });

    this.musicKit = MusicKit.getInstance();
    this.authorized = this.musicKit.isAuthorized;
  }
}
