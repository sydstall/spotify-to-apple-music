import { TestBed } from '@angular/core/testing';

import { MusicKitService } from './music-kit.service';

describe('MusicKitService', () => {
  let service: MusicKitService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MusicKitService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
