import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  API_URL = environment.API_URL + '/auth';

  constructor(private httpClient: HttpClient) { }

  getAppleMusicToken() {
  	return this.httpClient.get(this.API_URL + '/apple-music/tokens/create', { observe: 'response' });
  }

  getSpotifyToken() {
  	return this.httpClient.get(this.API_URL + '/spotify/tokens/create', { observe: 'response' });
  }

  getSpotifyClientId() {
  	return this.httpClient.get(this.API_URL + '/spotify/client-id/retrieve', { observe: 'response' });
  }
}
