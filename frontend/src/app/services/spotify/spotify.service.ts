import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SpotifyService {
  API_URL = environment.API_URL + '/spotify';

  constructor(private httpClient: HttpClient) { }

  getUserPlaylists(userToken) {
  	const payload = {
  	  token: userToken
  	};

  	return this.httpClient.post(this.API_URL + '/playlists/user/retrieve', payload, { observe: 'response' });
  }

  getPlaylist(developerToken, playlist) {
  	const payload = {
  	  token: developerToken,
  	  playlistId: playlist.id
  	};

  	return this.httpClient.post(this.API_URL + '/playlists/retrieve', payload, { observe: 'response' });
  }
}
