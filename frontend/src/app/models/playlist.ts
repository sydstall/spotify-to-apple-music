class Playlist {

  name: string;
  songs: array;

  constructor(playlist: object) {
  	this.name = playlist.name;
  	this.songs = playlist.songs;
  }

  setName(name: string) {
  	this.name = name;
  }

  setSongs(songs: array) {
  	this.songs = songs;
  }

  getName() {
  	return this.name;
  }

  getSongs() {
  	return this.songs;
  }
}